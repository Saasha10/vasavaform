import React from 'react'
import '../App.css'
import { Formik } from 'formik'

const initialFormValues = {
    name: '',
    surname: '',
    email: '',
    phone: '',
}

const onSubmit = (values, { setSubmitting }) => {
    alert(JSON.stringify(values, null, 2))
    setSubmitting(false)
}

const isValidEmail = values => !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)

const isValidPhone = values => /^[\]?[(]?[0-9]{3}[)]?[-\s\]?[0-9]{3}[-\s\]?[0-9]{6,6}$/im.test(values.phone)

const validateValues = values => {
    const errors = {}
    Object.keys(values).forEach(key => {
        const isEmail = key === 'email'
        const isPhone = key === 'phone'

        if(!values[key]){
            errors[key] = 'Required'
        }
        else if(isEmail && isValidEmail(values)){
            errors[key] = 'Invalid email address'
        }
        else if(isPhone && isValidPhone(values)){
            errors[key] = 'Invalid phone'
        }
    })
    return errors
}

export default function Form() {
    return (
        <div>
            <Formik
                initialValues={initialFormValues}
                validate={values => validateValues(values)}
                onSubmit={onSubmit}>
                {({
                    values,
                    errors,
                    handleChange,
                    handleSubmit,
                    isSubmitting,
                }) => (

                    <form className="formikForm" onSubmit={handleSubmit}>
                        <div className="row">
                            <div className="column">
                                <label className="label" htmlFor="name">Name</label>
                                <input
                                    className="input"
                                    id="name"
                                    name="name"
                                    type="text"
                                    onChange={handleChange}
                                    value={values.name}
                                />
                                {errors.name && <span className="required">* {errors.name}</span>}
                            </div>
                            <div className="column">
                                <label className="label" htmlFor="surname">Surname</label>
                                <input
                                    className="input"
                                    id="surname"
                                    name="surname"
                                    type="text"
                                    onChange={handleChange}
                                    value={values.surname}
                                />
                                {errors.surname && <span className="required">* {errors.surname}</span>}
                            </div>
                        </div>
                        <div className="row">
                            <div className="column">
                                <label className="label" htmlFor="email">Email Address</label>
                                <input
                                    className="input"
                                    id="email"
                                    name="email"
                                    type="email"
                                    onChange={handleChange}
                                    value={values.email}
                                />
                                {errors.email && <span className="required">* {errors.email}</span>}
                            </div>
                            <div className="column">
                                <label className="label" htmlFor="phone">Phone</label>
                                <input
                                    className="input"
                                    id="phone"
                                    name="phone"
                                    type="number"
                                    onChange={handleChange}
                                    value={values.phone}
                                />
                                {errors.phone && <span className="required">* {errors.phone}</span>}
                            </div>
                        </div>
                        <div className="row center">
                            <button type="submit" className="btn-submit" id="submitButton" disabled={isSubmitting}>Submit</button>
                        </div>
                    </form>
                )}
            </Formik>
        </div>
    )
}
